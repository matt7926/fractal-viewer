#include <SFML/OpenGL.hpp>
#include <SFML/Graphics.hpp>
#include <cmath>
#include <iostream>
#include <string>
#include <unistd.h>

int zoom(double *, double *, double *, double *, int);
void pan(double *, double *, double *, double *, int);
void updateView(sf::RenderWindow *, sf::Shader *, sf::Sprite *);
void setupWindow(sf::RenderWindow *);
void setupSprite(sf::Texture *, sf::Sprite *, int, int);
void setupShader(sf::Shader *, int, sf::Glsl::Vec2, sf::Glsl::Vec4);

int main(int argc, char ** argv) {
    //Variables
    double realMin = -2.0, realMax = 2.0, imagMin = -2, imagMax = 2, cReal = -0.1, cImag = 0.651;
    int width  = 1000, height = 1000, aa = 4, maxIter = 500;
    char * filePath = NULL;
    bool running = true;
    sf::Shader fractal;
    sf::Texture fractalTex;
    sf::Sprite fractalSprite;
    sf::ContextSettings settings;

    //Options parsing
    char c = 0;
    while ((c = getopt(argc, argv, "r:i:a:n:")) != -1) {
        switch(c) {
            case 'r':
                cReal = atof(optarg);
                break;
            case 'i':
                cImag = atof(optarg);
                break;
            case 'a':
                settings.antialiasingLevel = atoi(optarg);
                break;
            case 'n':
                maxIter = atoi(optarg);
                break;
        }
    }
    if (argv[optind] == NULL) {
        std::cerr << "No shader file specified" << std::endl;
        return EXIT_FAILURE;
    } else {
        filePath = argv[optind];
    }
    
    //Setup
    if (!fractal.loadFromFile(filePath, sf::Shader::Fragment)) {
        return EXIT_FAILURE;
    }
    sf::RenderWindow window(sf::VideoMode(width, height), "Fractal Viewer", sf::Style::Titlebar | sf::Style::Close , settings);
    setupWindow(&window);
    setupSprite(&fractalTex, &fractalSprite, width, height);
    setupShader(&fractal, maxIter, sf::Glsl::Vec2(cReal, cImag), sf::Glsl::Vec4(realMin, realMax, imagMin, imagMax));
    updateView(&window, &fractal, &fractalSprite);
    
    //Event loop
    while (running) {
        sf::Event event;
        while (window.pollEvent(event)) {
            //Window closed
            if (event.type == sf::Event::Closed) {
                running = false;
            }

            //Window Resized
            else if (event.type == sf::Event::Resized) {
                glViewport(0, 0, event.size.width, event.size.height);
            }

            //Mouse scrolled
            else if (event.type == sf::Event::MouseWheelScrolled) {
                if (event.mouseWheelScroll.wheel == sf::Mouse::VerticalWheel) {
                    if(zoom(&realMin, &realMax, &imagMin, &imagMax, event.mouseWheelScroll.delta)) {
                        fractal.setUniform("bounds", sf::Glsl::Vec4(realMin, realMax, imagMin, imagMax));
                        updateView(&window, &fractal, &fractalSprite);
                    }
                }
            }

            //Key pressed
            else if (event.type == sf::Event::KeyPressed) {
                if (71 <= event.key.code && event.key.code <= 74) {
                    pan(&realMin, &realMax, &imagMin, &imagMax, event.key.code);
                    fractal.setUniform("bounds", sf::Glsl::Vec4(realMin, realMax, imagMin, imagMax));
                    updateView(&window, &fractal, &fractalSprite);
                    
                }
            }
        }
    }
    return EXIT_SUCCESS;
}

int zoom(double * realMin, double * realMax, double * imagMin, double * imagMax, int scrollDir) {
    double realDelta = *realMax - *realMin;
    double imagDelta = *imagMax - *imagMin;
    if (realDelta >= 0.00002 || scrollDir == -1) {
        *realMin += scrollDir * realDelta / 16; 
        *realMax -= scrollDir * realDelta / 16;
        *imagMin += scrollDir * imagDelta / 16;
        *imagMax -= scrollDir * imagDelta / 16;
        return 1;
    }
    return 0;
}

void pan(double * realMin, double * realMax, double * imagMin, double * imagMax, int keyCode) {
    double realDelta = *realMax - *realMin;
    double imagDelta = *imagMax - *imagMin;
    switch (keyCode) {
        case 73:
            *imagMin -= imagDelta / 32;
            *imagMax -= imagDelta / 32;
            break;
        case 74:
            *imagMin += imagDelta / 32;
            *imagMax += imagDelta / 32;
            break;
        case 71:
            *realMin -= realDelta / 32; 
            *realMax -= realDelta / 32;
            break;
        case 72:
            *realMin += realDelta / 32; 
            *realMax += realDelta / 32;
            break;
    }
}

void updateView(sf::RenderWindow * window, sf::Shader * shader, sf::Sprite * sprite) {
    window->clear();
    window->draw(*sprite, shader);
    window->display();
}

void setupWindow(sf::RenderWindow * window) {
    window->setVerticalSyncEnabled(true);
    window->setFramerateLimit(60);
    window->setActive();
}

void setupSprite(sf::Texture * tex, sf::Sprite * spr, int width, int height) {
    tex->create(width, height);
    tex->setSmooth(true);
    *spr = sf::Sprite(*tex);
}

void setupShader(sf::Shader * shader, int iter, sf::Glsl::Vec2 c, sf::Glsl::Vec4 bounds) {
    shader->setUniform("iter", iter);
    shader->setUniform("bounds", bounds);
    shader->setUniform("c", c);
}