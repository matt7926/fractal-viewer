CC = g++
BIN = bin/
SRC = src/
FLAGS = -lsfml-graphics -lsfml-window -lsfml-system -lGL

fractalViewer: $(BIN)main.o
	$(CC) $(BIN)main.o  -o fractalViewer $(FLAGS)

$(BIN)main.o: $(SRC)main.cpp
	$(CC) -c $(SRC)main.cpp -o $(BIN)main.o
