#extension GL_ARB_gpu_shader_fp64 : enable

uniform vec2 c;
uniform vec4 bounds;
uniform int iter;

dvec2 complex_square(dvec2 a) {
	return dvec2(a.x * a.x - a.y * a.y, a.x * a.y + a.y * a.x);
}

void main() {
	dvec2 z;
	z.x = (bounds.y - bounds.x) * gl_TexCoord[0].x + bounds.x;
	z.y = (bounds.w - bounds.z) * gl_TexCoord[0].y + bounds.z;
	int i;
	for(i = 0; i < iter; i++) {
		z = complex_square(z) + c;
		if ((z.x * z.x + z.y * z.y) > 4.0)  {
			break;
		}
	}
	double t = double(i) / double(iter);
	double r = 9.0 * (1.0 - t) * t * t * t;
  	double g = 15.0 * (1.0 - t) * (1 - t) * t * t;
  	double b = 8.5 * (1.0 - t) * (1 - t) * (1 - t) * t;
	gl_FragColor = dvec4(r, g, b, 1.0);
}