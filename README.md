## Compilation

Compile with `make fractalViewer`

## Usage
Run with `./fractalViewer [flags] shaderFile` 

**Flags**
* `-r`: The real part of the c value for Julia sets
* `-i`: The imaginary part of the c value for Julia sets
* `-a`: The level of antialiasing
* `-n`: The maximun number of iterations

Zoom with the scroll wheel and pan with arrow keys
